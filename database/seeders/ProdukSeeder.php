<?php

namespace Database\Seeders;

use App\Models\Produk;
use Illuminate\Database\Seeder;

class ProdukSeeder extends Seeder
{
    public function run()
    {
        $json = file_get_contents('https://gist.githubusercontent.com/FastPrintProg3/dec91c65f573d09a6e7836c65ae54e73/raw');
        $datas = json_decode($json);
        foreach ($datas as $data) {
            $a = new Produk;
            $a->id=$data->id_produk;
            $a->nama=$data->nama_produk;
            $a->kategori=$data->kategori;
            $a->harga=(float)$data->harga;
            if ($data->status=='bisa dijual') {
                $a->dijual=true;
            }
            else {
                $a->dijual=false;
            }
            $a->save();
        }
    }
}
