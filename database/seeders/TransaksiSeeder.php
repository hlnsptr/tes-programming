<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransaksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transaksis')->insert([
            'id' => 1,
            'tanggal_order'=>'2020-12-01 11:30:00',
            'status' => 'lunas',
            'tanggal_pembayaran' => '2020-12-01 12:00:00',
        ]);
        DB::table('transaksis')->insert([
            'id' => 2,
            'tanggal_order'=>'2020-12-02 10:30:00',
            'status' => 'pending',
            'tanggal_pembayaran' => null,
        ]);
    }
}
