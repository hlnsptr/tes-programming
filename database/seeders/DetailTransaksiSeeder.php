<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DetailTransaksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('detail_transaksis')->insert([
            'id' => 1,
            'transaksi_id'=>1,
            'harga' => 10000,
            'jumlah' => 2,
            'sub_total' => 20000,
        ]);
        DB::table('detail_transaksis')->insert([
            'id' => 2,
            'transaksi_id'=>2,
            'harga' => 6250,
            'jumlah' => 4,
            'sub_total' => 25000,
        ]);
    }
}
