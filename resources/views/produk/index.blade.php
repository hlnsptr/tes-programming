@extends('layout',['title'=>'Produk'])
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <button type="button" class="btn btn-primary btn-sm" id="tambah-data"><i class="fas fa-plus"></i> Tambah Data</button>
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="table-data" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>#ID</th>
                                        <th>Nama</th>
                                        <th>Kategori</th>
                                        <th>Harga</th>
                                        <th>Status</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
    </div>
    @include('produk.create')
    @include('produk.edit')
@endsection
@section('script_under')
<script>
$(document).ready(function() {
    var table = $('#table-data').DataTable({
		processing:true,
		serverSide:true,
		ajax:'{{ route('produk.datatable') }}',
		"order": [[ 0, "asc" ]],
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		columns:[
            {data:'id', name:'id'},
			{data:'nama', name:'nama'},
			{data:'kategori', name:'kategori'},
            {data:'harga', name:'harga'},
			{data:'status', name:'dijual',searchable:false},
			{data:'action',name:'action',orderable:false,searchable:false},
		],
        "columnDefs": [
            {className: "text-center", "targets": [4]}
        ]
	});
    var rulesCreate={
            nama: {
                required: true,
            },
            kategori: {
                required: true,
            },
            harga: {
                required: true,
                number:true,
            },
            status: {
                required: true,
            },
        }
    var formCreate = $( "#form-create" ).validate({
        rules: rulesCreate,
        submitHandler: function (form) {
            $('#modal-create').LoadingOverlay("show");
            $.ajax({
                type: form.method,
                url: form.action,
                data: $(form).serialize(),
                complete: function (data) {
                    $('#modal-create').LoadingOverlay("hide");
                },
                success: function (response) {
                    Swal.fire({
                        icon: 'success',
                        title: response.message,
                        showConfirmButton: true,
                        showCloseButton: true,
                        timer:1500,
                    });
                    $('#modal-create').modal('hide');
                    table.ajax.reload();
                },
                error: function (request, status, error) {
                    Swal.fire({
                        icon: 'error',
                        title: request.responseJSON.message,
                        showConfirmButton: true,
                        showCloseButton: true,
                    });
                }
            });
        }
    });
    $('#tambah-data').click(function (e) { 
        e.preventDefault();
        $("#form-create").find('.is-invalid').removeClass('is-invalid');
        $('#modal-create').modal('show');
        $( "#form-create")[0].reset();
        formCreate.resetForm();
    });
    var rulesEdit={
        nama: {
            required: true,
        },
        kategori: {
            required: true,
        },
        harga: {
            required: true,
            number:true,
        },
        status: {
            required: true,
        },
    }
    var formEdit = $( "#form-edit" ).validate({
        rules: rulesEdit,
        submitHandler: function (form) {
            $('#modal-edit').LoadingOverlay("show");
            $.ajax({
                type: form.method,
                url: form.action,
                data: $(form).serialize(),
                complete: function (data) {
                    $('#modal-edit').LoadingOverlay("hide");
                },
                success: function (response) {
                    Swal.fire({
                        icon: 'success',
                        title: response.message,
                        showConfirmButton: true,
                        showCloseButton: true,
                        timer:1500,
                    });
                    $('#modal-edit').modal('hide');
                    table.ajax.reload();
                },
                error: function (request, status, error) {
                    Swal.fire({
                        icon: 'error',
                        title: request.responseJSON.message,
                        showConfirmButton: true,
                        showCloseButton: true,
                    });
                }
            });
        }
    });
    $("#table-data").on('click','.edit', function (e) {
        e.preventDefault();
        $("#form-edit").find('.is-invalid').removeClass('is-invalid');
        var id = $(this).attr('id');
        var urlUpdate = "{{route('produk.update',['produk'=>':id'])}}";
        $( "#form-edit" ).attr('action',urlUpdate);
        urlUpdate = $( "#form-edit" ).attr('action').replace(':id',id);
        $( "#form-edit" ).attr('action',urlUpdate);
        var urlEdit = "{{route('produk.edit',['produk'=>':id'])}}";
        urlEdit = urlEdit.replace(':id',id);
        $.ajax({
            type: "GET",
            url: urlEdit,
            dataType: "JSON",
            success: function (response) {
                $('#modal-edit').modal('show');
                $( "#form-edit" )[0].reset();
                formEdit.resetForm();
                $('#nama-edit').val(response.data.nama);
                $('#kategori-edit').val(response.data.kategori);
                $('#harga-edit').val(response.data.harga);
                if (response.data.dijual==1) {
                    $('#status-1-edit').attr('checked', 'checked');
                }
                else if(response.data.dijual==0){
                    $('#status-2-edit').attr('checked', 'checked');
                }
            },
            error: function (request, status, error) {
                Swal.fire({
                    icon: 'error',
                    title: request.responseJSON.message,
                    showConfirmButton: true,
                    showCloseButton: true,
                });
            }
        });
    });
    $("#table-data").on('click','.hapus', function (e) {
        e.preventDefault();
        var id = $(this).attr('id');
        var urlDelete = "{{route('produk.destroy',['produk'=>':id'])}}";
        urlDelete = urlDelete.replace(':id',id);
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "ini adalah hapus permanen!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus!'
            }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: "POST",
                    url: urlDelete,
                    data: {_method:'DELETE',_token:'{{csrf_token()}}'},
                    dataType: "JSON",
                    success: function (response) {
                        Swal.fire({
                        icon: 'success',
                        title: response.message,
                        showConfirmButton: true,
                        showCloseButton: true,
                        timer:1500,
                        });
                        table.ajax.reload();
                    },
                    error: function (request, status, error) {
                        Swal.fire({
                        icon: 'error',
                        title: request.responseJSON.message,
                        showConfirmButton: true,
                        showCloseButton: true,
                        });
                    }
                });
            }
        });
    });
});
</script>
@endsection