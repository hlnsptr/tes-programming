<div class="modal fade" id="modal-edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Form Edit Data</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form action="" method="POST" id="form-edit">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="nama-edit">Nama</label>
                            <input type="text" class="form-control" id="nama-edit" name="nama">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="kategori-edit">Kategori</label>
                            <input type="text" class="form-control" id="kategori-edit" name="kategori">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="harga-edit">Harga</label>
                            <input type="text" class="form-control" id="harga-edit" name="harga">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="status-edit">Status</label>
                            <div>
                                <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="status" id="status-1-edit" value="1">
                                <label class="form-check-label" for="status-1-edit">Bisa Dijual</label>
                                </div>
                                <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="status" id="status-2-edit" value="0">
                                <label class="form-check-label" for="status-2-edit">Tidak Bisa Dijual</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{csrf_field()}}
                {{method_field('PATCH')}}
              
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>