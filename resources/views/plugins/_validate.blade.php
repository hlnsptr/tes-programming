@if($type=='js')
<script src="{{asset('plugins/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('plugins/jquery-validation/additional-methods.min.js')}}"></script>
<script src="{{asset('plugins/jquery-validation/localization/messages_id.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $.validator.setDefaults({
            ignore: [], 
            errorElement: 'span',
            errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                if ($(element).attr('type') == "radio") {
                } else {
                    $(element).addClass('is-invalid');
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                if ($(element).attr('type') == "radio") {
                } else {
                    $(element).removeClass('is-invalid');
                }
            }
		});
    });
</script>
@endif