<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TesController extends Controller
{
    public function palindrome(Request $request){
        function getPalindrome($text){
            $text = preg_replace('/\s+/', '', $text);
            return $text;
        }
        function printSubStr($str,$low,$high){
            $data=[];
            for ($i=$low; $i < $high; $i++) { 
                $data[]=$str[$i];
            }
            $txt=implode($data);
            $length=strlen($txt)/2;
            $txt1=substr($txt,0,$length);
            $txt2=substr($txt,$length);
            return $txt1.' '.$txt2;
        }
        function longestPalSubstr($str)
        {
            $n = strlen($str);
            $maxLength = 1;
            $start = 0;
            for ($i = 0; $i < $n; $i++) {
                for ($j = $i; $j < $n; $j++) {
                    $flag = 1;
                    for ($k = 0; $k < ($j - $i + 1) / 2; $k++)
                        if ($str[$i + $k] != $str[$j - $k])
                            $flag = 0;
                    if ($flag!=0 && ($j - $i + 1) > $maxLength) {
                        $start = $i;
                        $maxLength = $j - $i + 1;
                    }
                }
            }
            $data=printSubStr($str, $start, $start + $maxLength);
            return $data;
        }
        $oriText=getPalindrome(strtolower($request->palindromeText));
        if ($request->has('palindromeText')) {
            $dataPalindrome=longestPalSubstr($oriText);
        }
        else{
            $dataPalindrome='';
        }
        return view('tes.palindrome',compact('dataPalindrome'));
    }
    public function api(){
        function binaryToDecimal($binary){
            $base=1;
            $decimal=0;
            $binary=explode(",", preg_replace("/(.*),/", "$1", str_replace("1", "1,", str_replace("0", "0,", $binary))));
            for($i=1; $i<count($binary); $i++) $base=$base*2;
            foreach($binary as $key=>$bin_nr_bit) {
                if($bin_nr_bit==1) {
                    $decimal+=$base;
                    $base=$base/2;
                }
                if($bin_nr_bit==0) $base=$base/2;
            }
            return $decimal;
        }
        function decimalToBinary($decimal){
            if ($decimal == 0) return 0;
         
            $flag = array();
            while ($decimal != 0) {
                array_push($flag, $decimal % 2);
                $decimal = (int)($decimal / 2);
            }
            $result = '';
            while (!empty($flag)) {
                $result .= array_pop($flag);
            }
            return $result;
        }
        return response()->json([
            "1001"=>binaryToDecimal(1001),
            "19"=>(int) decimalToBinary(19),
        ], 200);
    }
    public function index()
    {
        $transaksis=DB::table('transaksis')
        ->join('detail_transaksis', 'transaksis.id', '=', 'detail_transaksis.transaksi_id')
        ->selectRaw('transaksis.id,transaksis.tanggal_order,transaksis.status,transaksis.tanggal_pembayaran,detail_transaksis.sub_total,detail_transaksis.jumlah')
        ->get();
        return view('tes.index',compact('transaksis'));
    }
}
