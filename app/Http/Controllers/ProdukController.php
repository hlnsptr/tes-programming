<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ProdukController extends Controller
{
    
    public function index()
    {
        return view('produk.index');
    }
    public function datatable(Request $request)
    {
        $a=Produk::selectRaw('produks.*,IF(produks.dijual=1, "Bisa Dijual", "Tidak Bisa Dijual") as status')
        ->where('dijual',1);
        $datatables = DataTables::of($a);
        return $datatables->addColumn('action',function($a){
            $dropdown='<div class="btn-group dropleft">
                <button type="button" class="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-ellipsis-h"></i>
                </button>
                <div class="dropdown-menu">
                <a class="dropdown-item edit" href="#" id="'.$a->id.'">Edit</a>
                <a class="dropdown-item hapus" href="#" id="'.$a->id.'">Hapus</a>
                </div>
            </div';
            return $dropdown;
        })->addColumn('harga',function($a){
            return number_format($a->harga,2,',','.');
        })->rawColumns(['action'])->make(true);
    }
    public function store(Request $request)
    {
        $rules=[
            'nama' => 'required',
            'harga' => 'required|numeric',
            'kategori' => 'required',
            'status'=>'required',
        ];
        $request->validate($rules);
        $a=new Produk();
        $a->nama=$request->nama;
        $a->harga=$request->harga;
        $a->kategori=$request->kategori;
        $a->dijual=$request->status;
        if ($a->save()) {
            return response()->json([
                "message"=>"Data berhasil ditambahkan !",
            ], 200);
        }
        return response()->json([
            "message"=>"Data gagal ditambahkan !",
        ], 500);
    }
    public function edit($id)
    {
        $a=Produk::where('id',$id)->firstOrFail();
        return response()->json([
            "data"=>$a,
            "message"=>"",
        ], 200);
    }
    public function update(Request $request, $id)
    {
        $rules=[
            'nama' => 'required',
            'harga' => 'required|numeric',
            'kategori' => 'required',
            'status'=>'required',
        ];
        $request->validate($rules);
        $a=Produk::findOrFail($id);
        $a->nama=$request->nama;
        $a->harga=$request->harga;
        $a->kategori=$request->kategori;
        $a->dijual=$request->status;
        if ($a->update()) {
            return response()->json([
                "message"=>"Data berhasil diubah !",
            ], 200);
        }
        return response()->json([
            "message"=>"Data gagal diubah !",
        ], 500);
    }
    public function destroy($id)
    {
        $a=Produk::findOrFail($id);
        if ($a->delete()) {
            return response()->json([
                "message"=>"Data berhasil dihapus !",
            ], 200);
        }
        return response()->json([
            "message"=>"Data gagal dihapus !",
        ], 500);
    }
}
