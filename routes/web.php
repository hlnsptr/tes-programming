<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TesController;
use App\Http\Controllers\ProdukController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::prefix('produk')->group(function () {
    Route::get('/datatable',[ProdukController::class, 'datatable'])->name('produk.datatable');
});
Route::prefix('tes')->group(function () {
    Route::get('/',[TesController::class, 'index'])->name('tes.index');
    Route::get('/api',[TesController::class, 'api'])->name('tes.api');
    Route::get('/palindrome',[TesController::class, 'palindrome'])->name('tes.palindrome');
});
Route::resources([
    'produk' => ProdukController::class,
]);
